var express = require('express');
var stylus = require('stylus');

var app = express();

app.use(stylus.middleware(process.argv[3]));

app.get('/main.css', function(req, res)
{
	res.sendFile(process.argv[3] + '\\main.css');
});


app.listen(process.argv[2]);